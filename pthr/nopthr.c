#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define SIZE 10000000000

clock_t start,end;
double t;



int main(int argc, char **argv) {
	start= clock();
	long i;
	for(i=0; i<SIZE*2; i++) {
		//printf("Thread 000000000 - value %d\n", i);
	}
	end=clock();
	printf("CLOCKS until join: %.5lf\n",(double)(end-start));
	printf("time until join: %.6lf\n",(double)(end-start)/CLOCKS_PER_SEC);
		
}
