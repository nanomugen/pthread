#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

//funciona a recursão
pthread_t thr;
void* func(void *arg){
	printf("dentro da func\n");
	int* a= (int *) arg;
	printf("inside thread: %d of func. Loop: %d\n",pthread_self(),*a);
	if(*a>10) return NULL;
	
	*a=(*a)+1;
	pthread_t newthr;
	pthread_create(&newthr,NULL,&func,a);
	pthread_join(newthr,NULL);
	return NULL;	
}

int main(){
	int a = 0;
	int b;
	
	printf("inside main: %d\n",a);	
	b = pthread_create(&thr,NULL,func,&a);
	if(b!=0){
	printf("erro\n");
	}
	pthread_join(thr,NULL);
	printf("fim\n");
	return 0;
}

