#include <stdio.h>
#define MAX 10000

int main(int argc, char **argv){
  int i;
  printf("struct Rect rectGen[] = {\n" );
  for(i=0;i<MAX;i++)
    printf("{%d, %d, %d, %d},\n",i,i+1,i+2,i+3);
  printf("}\n" );
  return 0;
}
