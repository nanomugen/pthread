#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#define SIZE 100
#define THRNUM 2
clock_t start,end;
double t;
typedef struct {
	long idx, length;
}thread_arg, *ptr_thread_arg;

pthread_t threads[THRNUM];

void *thread_func(void *arg) {
	ptr_thread_arg targ = (ptr_thread_arg)arg;
	long i;
	for(i=targ->idx; i<(targ->idx + targ->length); i++) {
		printf("Thread %d - value %d\n", pthread_self(), i);
	}
}

int main(int argc, char **argv) {
	printf("SIZE: %ld\n",SIZE);
	start= clock();
	thread_arg arguments[THRNUM];
	long i;
	for(i=0; i<THRNUM; i++) {
		arguments[i].idx = i * SIZE;
		arguments[i].length = SIZE;
		pthread_create(&(threads[i]), NULL, thread_func,
		&(arguments[i]));
	}
	end=clock();
	printf("CLOCKS until join: %.5lf\n",(double)(end-start));
	printf("time until join: %.6lf\n",(double)(end-start)/CLOCKS_PER_SEC);
	for(i=0; i<THRNUM; i++) {
		pthread_join(threads[i], NULL);
	}
	end=clock();
	printf("CLOCKS after join: %.5lf\n",(double)(end-start));
	printf("time after join: %.6lf\n",(double)(end-start)/CLOCKS_PER_SEC);
	printf("try reuse\n");
	for(i=0; i<THRNUM; i++) {
		pthread_create(&(threads[i]), NULL, thread_func,&(arguments[i]));
	}
	for(i=0; i<THRNUM; i++) {
		pthread_join(threads[i], NULL);
	}
}
