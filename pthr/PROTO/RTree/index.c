
#include <stdio.h>
#include <malloc.h>
#include "assert.h"
#include "index.h"
#include "card.h"


// Make a new index, empty.  Consists of a single node.
//
struct Node * RTreeNewIndex()
{
	struct Node *x;
	x = RTreeNewNode();
	x->level = 0; /* leaf */
	return x;
}



// Search in an index tree or subtree for all data retangles that
// overlap the argument rectangle.
// Return the number of qualifying data rects.
//


pthread_t* getFreeThread(){
	int i;
	printf("dentro do getfreethread\n");
	for(i=0; i < THRNUM;i++){
		if(flag[i]==0){
			flag[i]==1;
			return &thr[i];
		}
	}
	return NULL;
}

int RTreeSearch(struct Node *N, struct Rect *R, SearchHitCallback shcb, void* cbarg)
{
	register struct Node *n = N;
	register struct Rect *r = R; // NOTE: Suspected bug was R sent in as Node* and cast to Rect* here. Fix not yet tested.
	register int hitCount = 0;
	register int i;
	printf("dentro do RTREESEARCH\n");
	assert(n);
	assert(n->level >= 0);
	assert(r);

	if (n->level > 0) /* this is an internal node in the tree */
	{
		for (i=0; i<NODECARD; i++)
		if (n->branch[i].child &&
			RTreeOverlap(r,&n->branch[i].rect))
			{

				hitCount += RTreeSearch(n->branch[i].child, R, shcb, cbarg);
			}
		}
		else /* this is a leaf node */
		{
			for (i=0; i<LEAFCARD; i++)
			if (n->branch[i].child &&
				RTreeOverlap(r,&n->branch[i].rect))
				{
					hitCount++;
					if(shcb) // call the user-provided callback
					if( ! shcb((int)n->branch[i].child, cbarg))
					return hitCount; // callback wants to terminate search early
				}
			}
			return hitCount;
		}
		void* RTreeSearch2(void *data_void)
		{
			struct Search* data = (struct Search *) data_void;
			int *num[THRNUM];
			register struct Node *n = data->N;
			register struct Rect *r = data->R; // NOTE: Suspected bug was R sent in as Node* and cast to Rect* here. Fix not yet tested.

			register int hitCount = 0;
			register int i;
			printf("dentro do RTREESEARCH2\n");
			assert(n);
			printf("depois de assert n\n");
			assert(n->level >= 0);
			printf("depois de assert n->level\n");
			assert(r);
			printf("depois do assert\n");
			if (n->level > 0) /* this is an internal node in the tree */
			{
				printf("dentro do if do internal node\n");
				pthread_t* thr_list[THRNUM];
				int count=0,rstatus;
				printf("count = %d\n",count);
				for(i=0;i<THRNUM;i++){
					thr_list[i]=NULL;
					num[i]=(int *)malloc(sizeof(int));
				}
				printf("test\n");
				for (i=0; i<NODECARD; i++)
				if (n->branch[i].child &&
					RTreeOverlap(r,&n->branch[i].rect))
					{
						pthread_t *my_thread = getFreeThread();
						printf("/n");
						struct Search temp;
						temp.N= n->branch[i].child;
						temp.R= r;
						temp.shcb = data->shcb;
						temp.cbarg= data->cbarg;
						printf("depois de declarar o temp\n");
						printf("m\n");
						//IF NULL THERE IS NO FREE THREAD
						if(my_thread==NULL){
							printf("dentro do my_thread==null\n");
							hitCount += RTreeSearch(n->branch[i].child, data->R, data->shcb, data->cbarg);
							//void *numm =(RTreeSearch2((void*)temp));

							//hitCount += numm;
						}
						else{
							printf("dentro do else my_thread==null\ncount=%d\n",count);
							thr_list[count]= my_thread;

							/*tenta iniciar o thread, indicando a função 'routine' e passando como argumento a string "Minha primeira Thread!"*/
							rstatus = pthread_create (thr_list[count], NULL, &RTreeSearch2, num[count]);
							//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

							count++;
							printf("depois de pthread_create\ncount:%d\n",count);

						}
						if(thr_list[0]==NULL){
							printf("deu null\n");
						}
						else{
							printf("nao deu null\nn");
						}
						for(i=0;i<count;i++){
							printf("antes do join\nsizeof num:%d\n",sizeof(num));
							/*aguarda finalização do thread identificado por thread_id. O retorno é passado pelo ponteiro thread_res*/
							rstatus = pthread_join (*(thr_list+i), &(num[i]));
							printf("depois do join no %d-ésimo thr_list\n",i);
							hitCount+= *((int *) (num+i));
						}
					}


				}
				else /* this is a leaf node */
				{
					for (i=0; i<LEAFCARD; i++)
					if (n->branch[i].child &&
						RTreeOverlap(r,&n->branch[i].rect))
						{
							hitCount++;
							if(data->shcb) // call the user-provided callback
							if( ! data->shcb((int)n->branch[i].child, data->cbarg))
							return hitCount; // callback wants to terminate search early
						}
					}
					//return hitCount;
					pthread_exit(hitCount);
				}
//========================================================================================
				void* RTreeSearch3(void* arg){

					struct Search *data = (struct Search*) arg;
					struct Node *N = data->N;
					struct Rect *R = data->R;
					SearchHitCallback shcb = data->shcb;
					void* cbarg = data->cbarg;
					pthread_t searchThread[MAXTHR];
					int thrcount=0;

					register struct Node *n = N;
					register struct Rect *r = R; // NOTE: Suspected bug was R sent in as Node* and cast to Rect* here. Fix not yet tested.
					register int hitCount = 0;
					register int i;


					printf("%d>> AFTER REGISTERS\n",pthread_self());
					assert(n);
					assert(n->level >= 0);
					assert(r);

					if (n->level > 0) /* this is an internal node in the tree */
					{
						printf("%d>> NODE INTERNO\n",pthread_self());
						for (i=0; i<NODECARD; i++)
						if (n->branch[i].child &&
							RTreeOverlap(r,&n->branch[i].rect))
							{
								//PARALEL LOGIC HERE

								printf("%d>> overlap\n",pthread_self());
								struct Search newSearch;
								newSearch.N = n->branch[i].child;
								newSearch.R = R;
								printf("%d>> N/R\n",pthread_self());
								newSearch.shcb = shcb;
								newSearch.cbarg = cbarg;
								newSearch.hits = data->hits;
								if(threads<MAXTHR){

									printf("%d>> threads<MAXTHR\n",pthread_self());



									//newSearch.hits = malloc(sizeof(int));
									//*(newSearch.hits) = 0;
									printf("%d>> ANTES DO CREATE\n",pthread_self());
									int test;
									test = pthread_create(&(searchThread[thrcount]),NULL,&RTreeSearch3,&newSearch);
									if(test!=0)
										printf("%d>>@@@@@@@@@ DEU ERRO NA CRIACAO PTHREAD_CREATE\n",pthread_self());
									threads++;
									thrcount++;
									printf("%d>> depois do create\n",pthread_self());
								}
								else{
									printf("%d>> entrou no else do maxthread(sem paralelismo)\n",pthread_self());
									//hitCount += RTreeSearch(n->branch[i].child, R, shcb, cbarg);
									RTreeSearch3(&newSearch);

								}
							}
						}
						else /* this is a leaf node */
						{
							printf("%d>> ENTROU NO ELSE (LEAF NODE)\n",pthread_self());
							for (i=0; i<LEAFCARD; i++)
							if (n->branch[i].child && RTreeOverlap(r,&n->branch[i].rect))
							{
								//hitCount++;
								*data->hits +=1;
								if(shcb) // call the user-provided callback
								{
									if( ! shcb((int)n->branch[i].child, cbarg)){
									printf("%d>> ENTROU NO !SHCB\n",pthread_self());
									//return hitCount;
									return NULL;
									} // callback wants to terminate search early
								}
								printf("%d>> entrou no LEAF NODE mas nao no !shcb return\n",pthread_self());
							}

						}
						//****************************
						//ACERTAR O HITCOUNT NO NÓ FOLHA E FAZER A CONTAGEM PELO HIT DA STRUCT(TALVEZ DÊ RUIM POR COMPARTILHAR O ACESSO AO MESMO ENDEREÇO DO STRUCT)
						//VER SE DEU BOSTA NO JOIN
						printf("%d>>thrcount: %d\n",pthread_self(), thrcount);
						for(i=0;i<thrcount;i++){
							printf("*********antes do join noo thread %d\n",pthread_self());
							struct Search *result;
							int test2;
							test2 = pthread_join(searchThread[i],result);
							//test2 = pthread_join(searchThread[i],NULL);
							if(test2!=0)
								printf("########ERRO PTHREAD_JOIN\n");
							printf("%d>> | level of node: %d | Chegou aqui depois do join no índice %d\n",pthread_self(),N->level, i );
						}
						printf("%d>> tamanho do thrcount: %d | after for do join\n",pthread_self(),thrcount);
						//return hitCount;
						return NULL;
						}

						// Inserts a new data rectangle into the index structure.
						// Recursively descends tree, propagates splits back up.
						// Returns 0 if node was not split.  Old node updated.
						// If node was split, returns 1 and sets the pointer pointed to by
						// new_node to point to the new node.  Old node updated to become one of two.
						// The level argument specifies the number of steps up from the leaf
						// level to insert; e.g. a data rectangle goes in at level = 0.
						//
						static int RTreeInsertRect2(struct Rect *r,	int tid, struct Node *n, struct Node **new_node, int level)
						{
							/*
							register struct Rect *r = R;
							register int tid = Tid;
							register struct Node *n = N, **new_node = New_node;
							register int level = Level;
							*/

							printf("dentro do RTREEINSERT2\n");
							register int i;
							struct Branch b;
							struct Node *n2;

							assert(r && n && new_node);
							assert(level >= 0 && level <= n->level);

							// Still above level for insertion, go down tree recursively
							//
							if (n->level > level)
							{
								i = RTreePickBranch(r, n);
								if (!RTreeInsertRect2(r, tid, n->branch[i].child, &n2, level))
								{
									// child was not split
									//
									n->branch[i].rect =
									RTreeCombineRect(r,&(n->branch[i].rect));
									return 0;
								}
								else    // child was split
								{
									n->branch[i].rect = RTreeNodeCover(n->branch[i].child);
									b.child = n2;
									b.rect = RTreeNodeCover(n2);
									return RTreeAddBranch(&b, n, new_node);
								}
							}

							// Have reached level for insertion. Add rect, split if necessary
							//
							else if (n->level == level)
							{
								b.rect = *r;
								b.child = (struct Node *) tid;
								/* child field of leaves contains tid of data record */
								return RTreeAddBranch(&b, n, new_node);
							}
							else
							{
								/* Not supposed to happen */
								assert (FALSE);
								return 0;
							}
						}



						// Insert a data rectangle into an index structure.
						// RTreeInsertRect provides for splitting the root;
						// returns 1 if root was split, 0 if it was not.
						// The level argument specifies the number of steps up from the leaf
						// level to insert; e.g. a data rectangle goes in at level = 0.
						// RTreeInsertRect2 does the recursion.
						//
						int RTreeInsertRect(struct Rect *R, int Tid, struct Node **Root, int Level)
						{
							register struct Rect *r = R;
							register int tid = Tid;
							register struct Node **root = Root;
							register int level = Level;
							register int i;
							register struct Node *newroot;
							struct Node *newnode;
							struct Branch b;
							int result;

							printf("dentro do RTREEINSERT\n");
							assert(r && root);
							assert(level >= 0 && level <= (*root)->level);
							for (i=0; i<NUMDIMS; i++)
							assert(r->boundary[i] <= r->boundary[NUMDIMS+i]);

							if (RTreeInsertRect2(r, tid, *root, &newnode, level))  /* root split */
							{
								newroot = RTreeNewNode();  /* grow a new root, & tree taller */
								newroot->level = (*root)->level + 1;
								b.rect = RTreeNodeCover(*root);
								b.child = *root;
								RTreeAddBranch(&b, newroot, NULL);
								b.rect = RTreeNodeCover(newnode);
								b.child = newnode;
								RTreeAddBranch(&b, newroot, NULL);
								*root = newroot;
								result = 1;
							}
							else
							result = 0;

							return result;
						}




						// Allocate space for a node in the list used in DeletRect to
						// store Nodes that are too empty.
						//
						static struct ListNode * RTreeNewListNode()
						{
							return (struct ListNode *) malloc(sizeof(struct ListNode));
							//return new ListNode;
						}


						static void RTreeFreeListNode(struct ListNode *p)
						{
							free(p);
							//delete(p);
						}



						// Add a node to the reinsertion list.  All its branches will later
						// be reinserted into the index structure.
						//
						static void RTreeReInsert(struct Node *n, struct ListNode **ee)
						{
							register struct ListNode *l;

							l = RTreeNewListNode();
							l->node = n;
							l->next = *ee;
							*ee = l;
						}


						// Delete a rectangle from non-root part of an index structure.
						// Called by RTreeDeleteRect.  Descends tree recursively,
						// merges branches on the way back up.
						// Returns 1 if record not found, 0 if success.
						//
						static int
						RTreeDeleteRect2(struct Rect *R, int Tid, struct Node *N, struct ListNode **Ee)
						{
							register struct Rect *r = R;
							register int tid = Tid;
							register struct Node *n = N;
							register struct ListNode **ee = Ee;
							register int i;

							assert(r && n && ee);
							assert(tid >= 0);
							assert(n->level >= 0);

							if (n->level > 0)  // not a leaf node
							{
								for (i = 0; i < NODECARD; i++)
								{
									if (n->branch[i].child && RTreeOverlap(r, &(n->branch[i].rect)))
									{
										if (!RTreeDeleteRect2(r, tid, n->branch[i].child, ee))
										{
											if (n->branch[i].child->count >= MinNodeFill)
											n->branch[i].rect = RTreeNodeCover(
												n->branch[i].child);
												else
												{
													// not enough entries in child,
													// eliminate child node
													//
													RTreeReInsert(n->branch[i].child, ee);
													RTreeDisconnectBranch(n, i);
												}
												return 0;
											}
										}
									}
									return 1;
								}
								else  // a leaf node
								{
									for (i = 0; i < LEAFCARD; i++)
									{
										if (n->branch[i].child &&
											n->branch[i].child == (struct Node *) tid)
											{
												RTreeDisconnectBranch(n, i);
												return 0;
											}
										}
										return 1;
									}
								}



								// Delete a data rectangle from an index structure.
								// Pass in a pointer to a Rect, the tid of the record, ptr to ptr to root node.
								// Returns 1 if record not found, 0 if success.
								// RTreeDeleteRect provides for eliminating the root.
								//
								int RTreeDeleteRect(struct Rect *R, int Tid, struct Node**Nn)
								{
									register struct Rect *r = R;
									register int tid = Tid;
									register struct Node **nn = Nn;
									register int i;
									register struct Node *tmp_nptr;
									struct ListNode *reInsertList = NULL;
									register struct ListNode *e;

									assert(r && nn);
									assert(*nn);
									assert(tid >= 0);

									if (!RTreeDeleteRect2(r, tid, *nn, &reInsertList))
									{
										/* found and deleted a data item */

										/* reinsert any branches from eliminated nodes */
										while (reInsertList)
										{
											tmp_nptr = reInsertList->node;
											for (i = 0; i < MAXKIDS(tmp_nptr); i++)
											{
												if (tmp_nptr->branch[i].child)
												{
													RTreeInsertRect(
														&(tmp_nptr->branch[i].rect),
														(int)tmp_nptr->branch[i].child,
														nn,
														tmp_nptr->level);
													}
												}
												e = reInsertList;
												reInsertList = reInsertList->next;
												RTreeFreeNode(e->node);
												RTreeFreeListNode(e);
											}

											/* check for redundant root (not leaf, 1 child) and eliminate
											*/
											if ((*nn)->count == 1 && (*nn)->level > 0)
											{
												for (i = 0; i < NODECARD; i++)
												{
													tmp_nptr = (*nn)->branch[i].child;
													if(tmp_nptr)
													break;
												}
												assert(tmp_nptr);
												RTreeFreeNode(*nn);
												*nn = tmp_nptr;
											}
											return 0;
										}
										else
										{
											return 1;
										}
									}
