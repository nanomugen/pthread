
#include "index.h"
#include <stdio.h>

struct Rect rects[] = {
	// xmin, ymin, xmax, ymax (for 2 dimensional RTree)
	{0, 0, 1, 1},//0
	{1, 1, 2, 2},//1
	{2, 2, 3, 3},//2
	{3, 3, 4, 4},//3
	{4,4,5,5},//4
	{5,5,6,6},//5
	{6,6,7,7},//6
	{7,7,8,8},//7
	{8,8,9,9},//8
	{9,9,10,10},//9
	{10,10,11,11},//10
	{11,11,12,12},//11
	{12,12,13,13},//12
	{13,13,14,14},//13
	{14,14,15,15},//14
	{15,15,16,16},//15
	{16,16,17,17},//16
	{17,17,18,18},//17
	{19,19,20,20},//18
	{20,20,21,21},//19
	{21,21,22,22},//20
	{23,23,24,24},//21
	{24,24,25,25},//22
	{25,25,26,26},//23
	{7,5,7,5},//24
};
int nrects = sizeof(rects) / sizeof(rects[0]);
struct Rect search_rect3 = {
	{6, 4, 10, 6}, // search will find above rects that this one overlaps
};
struct Rect search_rect2 = {
	{0, 1, 2, 1}, // search will find above rects that this one overlaps
};
struct Rect search_rect = {
	{9.1, 9.1, 9.1, 9.1}, // search will find above rects that this one overlaps
};

int MySearchCallback(int id, void* arg)
{
	// Note: -1 to make up for the +1 when data was inserted
	printf("Hit data rect %d\n", id-1);
	return 1; // keep going
}

int main()
{
	struct Node* root = RTreeNewIndex();
	int i, nhits,nhits2,nhits3;
	threads = 0;
	//void* nhits3;
	printf("nrects = %d\n", nrects);
	/*
	 * Insert all the data rects.
	 * Notes about the arguments:
	 * parameter 1 is the rect being inserted,
	 * parameter 2 is its ID. NOTE: *** ID MUST NEVER BE ZERO ***, hence the +1,
	 * parameter 3 is the root of the tree. Note: its address is passed
	 * because it can change as a result of this call, therefore no other parts
	 * of this code should stash its address since it could change undernieth.
	 * parameter 4 is always zero which means to add from the root.
	 */

	for(i=0; i<nrects; i++)
		RTreeInsertRect(&rects[i], i+1, &root, 0); // i+1 is rect ID. Note: root can change
	nhits = RTreeSearch(root, &search_rect, MySearchCallback, 0);
	printf("Search resulted in %d hits\n", nhits);
	RTreePrintNode(root,0);
	printf("MAXCARD: %d\n", MAXCARD);

	printf("SEGUNDA PESQUISA:\n");
	nhits2 = RTreeSearch(root, &search_rect2, MySearchCallback, 0);
	printf("Search 2 resulted in %d hits\n\n\n", nhits2);

	printf("\nTERCEIRA PESQUISA:\n");

	struct Search temp;
	temp.N=root;
	temp.R=&search_rect3;
	temp.shcb=&MySearchCallback;
	temp.cbarg=NULL;
	int resposta =0;
	temp.hits = &resposta;
	nhits3 = RTreeSearch(root, &search_rect3, MySearchCallback, 0);
	pthread_t thread_main;
	pthread_create(&thread_main,NULL,RTreeSearch3,&temp);
	//nhits3 = RTreeSearch3(&temp);
	void* returns;
	int teste_main;
	teste_main = pthread_join(thread_main,NULL);
	if(teste_main!=0){
		printf("ERRO NO MAIN PTHREAD_JOIN\n");
	}
	else{
		printf("SEM ERRO NO MAIN PTHREAD_JOIN\n" );
	}
	//printf("Search 3 resulted in %d hits\n\n\n", *((int *)nhits3));
	printf("\nsize of RTREE: %d\nsize of a node: %d\nsize of a rect: %d\n size of branch: %d\n",sizeof(*root),sizeof(struct Node), sizeof(struct Rect),sizeof(struct Branch));
	printf("RTREESEARCH: %d | RTREESEARCH3: %d\n",nhits3,resposta );
	return 0;
}
