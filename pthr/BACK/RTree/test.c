
#include "index.h"
#include <stdio.h>

struct Rect rects2[] = {
	// xmin, ymin, xmax, ymax (for 2 dimensional RTree)
	{0, 0, 1, 1},//0
	{1, 1, 2, 2},//1
	{2, 2, 3, 3},//2
	{3, 3, 4, 4},//3
	{4,4,5,5},//4
	{5,5,6,6},//5
	{6,6,7,7},//6
	{7,7,8,8},//7
	{8,8,9,9},//8
	{9,9,10,10},//9
	{10,10,11,11},//10
	{11,11,12,12},//11
	{12,12,13,13},//12
	{13,13,14,14},//13
	{14,14,15,15},//14
	{15,15,16,16},//15
	{16,16,17,17},//16
	{17,17,18,18},//17
	{19,19,20,20},//18
	{20,20,21,21},//19
	{21,21,22,22},//20
	{23,23,24,24},//21
	{24,24,25,25},//22
	{25,25,26,26},//23
	{7,5,7,5},//24
};
struct Rect rects[] = {
{0, 0, 1, 1},{1, 1, 2, 2},
{2, 2, 3, 3},{3, 3, 4, 4},
{4, 4, 5, 5},{5, 5, 6, 6},
{6, 6, 7, 7},{7, 7, 8, 8},
{8, 8, 9, 9},{9, 9, 10, 10},
{10, 10, 11, 11},{11, 11, 12, 12},{12, 12, 13, 13},
{13, 13, 14, 14},{14, 14, 15, 15},
{15, 15, 16, 16},{16, 16, 17, 17},
{17, 17, 18, 18},{18, 18, 19, 19},
{19, 19, 20, 20},
};
int nrects = sizeof(rects) / sizeof(rects[0]);

struct Rect search_rect3 = {
	{0, 0, 1000, 1000}, // search will find above rects that this one overlaps
};

int MySearchCallback(int id, void* arg)
{
	// Note: -1 to make up for the +1 when data was inserted
	printf("%d>> Hit data rect %d\n",pthread_self(), id-1);
	return 1; // keep going
}

int main(int argc, char **argv){

	struct Node* root = RTreeNewIndex();
	int i, nhits,nhits2,nhits3;
	clock_t start, end;
	double nothr, timethr;
	threads = 0;
	//void* nhits3;
	printf("nrects = %d\n", nrects);
	/*
	 * Insert all the data rects.
	 * Notes about the arguments:
	 * parameter 1 is the rect being inserted,
	 * parameter 2 is its ID. NOTE: *** ID MUST NEVER BE ZERO ***, hence the +1,
	 * parameter 3 is the root of the tree. Note: its address is passed
	 * because it can change as a result of this call, therefore no other parts
	 * of this code should stash its address since it could change undernieth.
	 * parameter 4 is always zero which means to add from the root.
	 */

	for(i=0; i<nrects; i++)
		RTreeInsertRect(&rects[i], i+1, &root, 0); // i+1 is rect ID. Note: root can change

	struct Search temp;
	*temp.N=root;
	temp.R=search_rect3;
	temp.shcb=&MySearchCallback;
	temp.cbarg=NULL;
	int resposta =0;
	temp.hits = &resposta;
	start = clock();
	nhits3 = RTreeSearch(root, &search_rect3, MySearchCallback, 0);
	end = clock();
	nothr = (double)(end - start)/CLOCKS_PER_SEC;
	pthread_t thread_main;
	start = clock();
	pthread_create(&thread_main,NULL,RTreeSearch3,&temp);
	//nhits3 = RTreeSearch3(&temp);
	void* returns;
	int teste_main;
	teste_main = pthread_join(thread_main,NULL);
	if(teste_main!=0){
		printf("ERRO NO MAIN PTHREAD_JOIN\n");
	}
	else{
		printf("SEM ERRO NO MAIN PTHREAD_JOIN\n" );
	}
	end = clock();
	timethr = (double)(end - start)/CLOCKS_PER_SEC;
	printf("RTREESEARCH: %d | RTREESEARCH3: %d\nRTREESEARCH: %.6lf | RTREESEARCH3: %.6lf\n",nhits3,resposta, nothr, timethr);
	return 0;
}
