#include <stdio.h>
#define MAX 1000

int main(int argc, char **argv){
  int i;
  printf("struct Rect rectGen[] = {\n" );
  for(i=0;i<MAX;i++)
    printf("{%d, %d, %d, %d},\n",i,i,i+2,i+2);
  printf("}\n" );
  return 0;
}
