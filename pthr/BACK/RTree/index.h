#ifndef _INDEX_
#define _INDEX_

/* PGSIZE is normally the natural page size of the machine */
//#define PGSIZE	512
//CHANGIN DE DEFINE OG PGSIZE
#define PGSIZE	64
#define NUMDIMS	2	/* number of dimensions */
#define NDEBUG

#include <pthread.h>
#include <time.h>

typedef float RectReal;


/*-----------------------------------------------------------------------------
| Global definitions.
-----------------------------------------------------------------------------*/

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define NUMSIDES 2*NUMDIMS

#define MAXTHR 1
int threads;
pthread_t thr[MAXTHR];
int flag[MAXTHR];
struct Rect
{
	RectReal boundary[NUMSIDES]; /* xmin,ymin,...,xmax,ymax,... */
};

struct Node;

struct Branch
{
	struct Rect rect;
	struct Node *child;
};

/* max branching factor of a node */
#define MAXCARD (int)((PGSIZE-(2*sizeof(int))) / sizeof(struct Branch))

struct Node
{
	int count;
	int level; /* 0 is leaf, others positive */
	struct Branch branch[MAXCARD];
};

struct ListNode
{
	struct ListNode *next;
	struct Node *node;
};

/*
 * If passed to a tree search, this callback function will be called
 * with the ID of each data rect that overlaps the search rect
 * plus whatever user specific pointer was passed to the search.
 * It can terminate the search early by returning 0 in which case
 * the search will return the number of hits found up to that point.
 */
typedef int (*SearchHitCallback)(int id, void* arg);

struct Search
{
	struct Node N;
	struct Rect R;
	SearchHitCallback shcb;
	void* cbarg;
	int* hits;
};
pthread_t* getFreeThread();

int RTreeSearch(struct Node*, struct Rect*, SearchHitCallback, void*);
void* RTreeSearch2(void* data);
void* RTreeSearch3(void* data);
int RTreeInsertRect(struct Rect*, int, struct Node**, int depth);
int RTreeDeleteRect(struct Rect*, int, struct Node**);
struct Node * RTreeNewIndex();
struct Node * RTreeNewNode();
void RTreeInitNode(struct Node*);
void RTreeFreeNode(struct Node *);
void RTreePrintNode(struct Node *, int);
void RTreeTabIn(int);
struct Rect RTreeNodeCover(struct Node *);
void RTreeInitRect(struct Rect*);
struct Rect RTreeNullRect();
RectReal RTreeRectArea(struct Rect*);
RectReal RTreeRectSphericalVolume(struct Rect *R);
RectReal RTreeRectVolume(struct Rect *R);
struct Rect RTreeCombineRect(struct Rect*, struct Rect*);
int RTreeOverlap(struct Rect*, struct Rect*);
void RTreePrintRect(struct Rect*, int);
int RTreeAddBranch(struct Branch *, struct Node *, struct Node **);
int RTreePickBranch(struct Rect *, struct Node *);
void RTreeDisconnectBranch(struct Node *, int);
void RTreeSplitNode(struct Node*, struct Branch*, struct Node**);

int RTreeSetNodeMax(int);
int RTreeSetLeafMax(int);
int RTreeGetNodeMax();
int RTreeGetLeafMax();


#endif /* _INDEX_ */
